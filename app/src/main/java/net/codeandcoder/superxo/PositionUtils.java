package net.codeandcoder.superxo;

import net.codeandcoder.BasicTicTacToe.board.Position;

/**
 * Created by uge01006 on 30/03/2015.
 */
public class PositionUtils {

    public static int toOneCoordinate(Position pos) {
        int result = 0;

        switch (pos.getY()) {
            case 0:
                switch(pos.getX()) {
                    case 0:
                        result = 0;
                        break;
                    case 1:
                        result = 1;
                        break;
                    case 2:
                        result = 2;
                        break;
                }
                break;
            case 1:
                switch(pos.getX()) {
                    case 0:
                        result = 3;
                        break;
                    case 1:
                        result = 4;
                        break;
                    case 2:
                        result = 5;
                        break;
                }
                break;
            case 2:
                switch(pos.getX()) {
                    case 0:
                        result = 6;
                        break;
                    case 1:
                        result = 7;
                        break;
                    case 2:
                        result = 8;
                        break;
                }
                break;
        }

        return result;
    }

    public static Position toBothCoordinates(int pos) {
        Position result = null;

        switch (pos) {
            case 0:
                result = new Position(0,0);
                break;
            case 1:
                result = new Position(1,0);
                break;
            case 2:
                result = new Position(2,0);
                break;
            case 3:
                result = new Position(0,1);
                break;
            case 4:
                result = new Position(1,1);
                break;
            case 5:
                result = new Position(2,1);
                break;
            case 6:
                result = new Position(0,2);
                break;
            case 7:
                result = new Position(1,2);
                break;
            case 8:
                result = new Position(2,2);
                break;
        }

        return result;
    }

}
