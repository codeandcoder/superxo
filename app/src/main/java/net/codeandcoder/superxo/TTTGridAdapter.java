package net.codeandcoder.superxo;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.codeandcoder.BasicTicTacToe.board.Position;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by uge01006 on 30/03/2015.
 */
public class TTTGridAdapter extends BaseAdapter {

    private List<String> values;
    private Activity activity;
    private LayoutInflater li;
    {
        resetValues();
    }

    public TTTGridAdapter(Activity activity, LayoutInflater li) {
        this.activity = activity;
        this.li = li;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        String value = values.get(position);

        if (convertView == null) {
            convertView = li.inflate(R.layout.grid_item, parent, false);
            vh = new ViewHolder(convertView);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        vh.text.setText(value);

        return convertView;
    }

    public String getValue(int position) {
        return values.get(position);
    }

    public void setValue(int position, String value) {
        values.set(position, value);
        activity.runOnUiThread(new Runnable() {
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    public void resetValues() {
        values = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            values.add("-");
        }
    }

    private static class ViewHolder {

        View wholeView;
        TextView text;

        public ViewHolder(View view) {
            this.wholeView = view;
            this.text = (TextView) view.findViewById(R.id.textView);
        }
    }

}
