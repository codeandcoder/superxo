package net.codeandcoder.superxo;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    private EditText name;
    private Button vsDumb;
    private Button vsClever;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (EditText) findViewById(R.id.your_name);

        vsDumb = (Button) findViewById(R.id.vs_ai);
        vsDumb.setOnClickListener(this);

        vsClever = (Button) findViewById(R.id.vs_ai_pro);
        vsClever.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        String nameText = name.getText().toString();

        Intent intent = new Intent(this, MainGameActivity.class);
        intent.putExtra("NAME", nameText);

        if (v.equals(vsDumb)) {
            intent.putExtra("GAMETYPE", GameUtils.GameType.VSAI);
        } else if (v.equals(vsClever)) {
            intent.putExtra("GAMETYPE", GameUtils.GameType.VSAIPRO);
        }

        startActivity(intent);
    }
}
