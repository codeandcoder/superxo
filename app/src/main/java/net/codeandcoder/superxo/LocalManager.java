package net.codeandcoder.superxo;

import android.app.Activity;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Position;
import net.codeandcoder.BasicTicTacToe.game.Game;
import net.codeandcoder.BasicTicTacToe.io.InputManager;
import net.codeandcoder.BasicTicTacToe.io.OutputManager;
import net.codeandcoder.BasicTicTacToe.listeners.TurnListener;
import net.codeandcoder.BasicTicTacToe.players.Player;

/**
 * Created by uge01006 on 30/03/2015.
 */
public class LocalManager extends Manager {

    public LocalManager(MainGameActivity activity, TTTGridAdapter adapter) {
        super(activity, adapter);
    }

    @Override
    public void write(String s) {
        if (s.equalsIgnoreCase("Do you want to exit now? Y/N")) {
            activity.showExitDialog(false);
        }
    }

    @Override
    public String read() {
        if (inputBuffer == null) {
            synchronized (this) {
                try {
                    this.wait();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
        String[] coordinates = inputBuffer.split("-");
        inputBuffer = coordinates.length > 1 ? coordinates[1] : null;
        return coordinates[0];
    }

    @Override
    public void close() {
        // do nothing
    }

    @Override
    public void onTurnEnded(int i, Board board, Player player, Position position) {
        for (int x = 0; x < board.getSize(); x++) {
            for (int y = 0; y < board.getSize(); y++) {
                int pos = PositionUtils.toOneCoordinate(new Position(x,y));
                if (adapter.getValue(pos) != board.getSquare(x,y).getValue().toString()) {
                    String value = board.getSquare(x,y).getValue().toString().equalsIgnoreCase("void") ?
                            "-" : board.getSquare(x,y).getValue().toString();
                    adapter.setValue(pos, value);
                }
            }
        }
    }

}
