package net.codeandcoder.superxo;

import android.app.Activity;

import net.codeandcoder.BasicTicTacToe.game.ContinousHumanVsAdaptativeAIGame;
import net.codeandcoder.BasicTicTacToe.game.Game;
import net.codeandcoder.BasicTicTacToe.game.HumanVsAIBasicGame;
import net.codeandcoder.BasicTicTacToe.game.HumanVsSmartAIBasicGame;
import net.codeandcoder.BasicTicTacToe.game.TicTacToe;
import net.codeandcoder.BasicTicTacToe.io.InputManager;
import net.codeandcoder.BasicTicTacToe.io.OutputManager;

/**
 * Created by uge01006 on 30/03/2015.
 */
public class ManagerBuilder {

    public static Manager build(GameUtils.GameType type, MainGameActivity activity, TTTGridAdapter adapter) {
        Manager manager = null;

        switch (type) {
            case VSAI:
                manager = new LocalManager(activity, adapter);
                break;
            case VSAIPRO:
                manager = new LocalManager(activity, adapter);
                break;
            case VSAIADAPTATIVE:
                manager = new LocalManager(activity, adapter);
                break;
            case VSAILEARNING:
                // to do
                break;
            case VSHUMANLOCAL:
                // to do
                break;
            case VSHUMANREMOTE:
                // to do
                break;
        }

        TicTacToe.registerListener(manager);

        return manager;
    }


}
