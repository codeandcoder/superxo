package net.codeandcoder.superxo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import net.codeandcoder.BasicTicTacToe.board.Position;
import net.codeandcoder.BasicTicTacToe.game.Game;
import net.codeandcoder.BasicTicTacToe.game.Result;
import net.codeandcoder.BasicTicTacToe.players.Player;


public class MainGameActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private TextView resultText;
    private GridView tttGrid;
    private TTTGridAdapter gridAdapter;
    private Manager manager;
    private Game game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game);

        resultText = (TextView) findViewById(R.id.result_text);
        gridAdapter = new TTTGridAdapter(this, getLayoutInflater());
        tttGrid = (GridView) findViewById(R.id.ttt_grid);
        tttGrid.setAdapter(gridAdapter);
        tttGrid.setOnItemClickListener(this);

        GameUtils.GameType type = (GameUtils.GameType) getIntent().getSerializableExtra("GAMETYPE");

        manager = ManagerBuilder.build(type, this, gridAdapter);
        game = GameBuilder.build(type, manager);
        Thread gameThread = new Thread(new GameThread());
        gameThread.start();

        // Easter Egg
        String playerName = getIntent().getStringExtra("NAME");
        if (playerName.equalsIgnoreCase("señoritax") || playerName.equalsIgnoreCase("señorax")
                || playerName.equalsIgnoreCase("senoritax") || playerName.equalsIgnoreCase("senoritax")) {

            findViewById(R.id.deamon).setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Position pos = PositionUtils.toBothCoordinates(position);
        manager.inputBuffer = pos.getX() + "-" + pos.getY();
        synchronized (manager) {
            manager.notify();
        }
    }

    private class GameThread implements Runnable {

        @Override
        public void run() {
            Result gameResult = game.play();
            Player winner = gameResult.getWinner();
            String draw = getResources().getString(R.string.draw);
            String youWin = getResources().getString(R.string.you_win);
            String itWins = getResources().getString(R.string.itWins);
            final String winnerName = winner == null ? draw : winner.getName().equalsIgnoreCase("you") ?
                    youWin : itWins;
            runOnUiThread(new Runnable() {
                public void run() {
                    resultText.setText(winnerName);
                }
            });
            showExitDialog(true);
        }
    }

    public void showExitDialog(final boolean resetGame) {
        runOnUiThread(new Runnable() {
            public void run() {
                new AlertDialog.Builder(MainGameActivity.this)
                        .setTitle("Game")
                        .setMessage(getResources().getString(R.string.play_again))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (resetGame) {
                                    resultText.setText("");

                                    Thread gameThread = new Thread(new GameThread());
                                    gameThread.start();
                                } else {
                                    manager.inputBuffer = "N";
                                    synchronized (manager) {
                                        manager.notify();
                                    }
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (!resetGame) {
                                    manager.inputBuffer = "Y";
                                    synchronized (manager) {
                                        manager.notify();
                                    }
                                }
                                finish();
                            }
                        }).show();
            }
        });
    }
}
