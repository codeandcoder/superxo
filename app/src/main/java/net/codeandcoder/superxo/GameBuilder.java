package net.codeandcoder.superxo;

import net.codeandcoder.BasicTicTacToe.game.ContinousHumanVsAdaptativeAIGame;
import net.codeandcoder.BasicTicTacToe.game.Game;
import net.codeandcoder.BasicTicTacToe.game.HumanVsAIBasicGame;
import net.codeandcoder.BasicTicTacToe.game.HumanVsSmartAIBasicGame;
import net.codeandcoder.BasicTicTacToe.game.TicTacToe;

/**
 * Created by uge01006 on 30/03/2015.
 */
public class GameBuilder {

    public static Game build(GameUtils.GameType type, Manager manager) {
        Game game = null;

        switch (type) {
            case VSAI:
                game = new HumanVsAIBasicGame(manager, manager);
                break;
            case VSAIPRO:
                game = new HumanVsSmartAIBasicGame(manager, manager);
                break;
            case VSAIADAPTATIVE:
                game = new ContinousHumanVsAdaptativeAIGame(manager, manager);
                break;
            case VSAILEARNING:
                // to do
                break;
            case VSHUMANLOCAL:
                // to do
                break;
            case VSHUMANREMOTE:
                // to do
                break;
        }

        return game;
    }

}
