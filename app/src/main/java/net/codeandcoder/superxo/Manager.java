package net.codeandcoder.superxo;

import android.app.Activity;

import net.codeandcoder.BasicTicTacToe.game.Game;
import net.codeandcoder.BasicTicTacToe.io.InputManager;
import net.codeandcoder.BasicTicTacToe.io.OutputManager;
import net.codeandcoder.BasicTicTacToe.listeners.TurnListener;

/**
 * Created by uge01006 on 30/03/2015.
 */
public abstract class Manager implements InputManager, OutputManager, TurnListener {

    public String inputBuffer;
    protected MainGameActivity activity;
    protected TTTGridAdapter adapter;

    public Manager(MainGameActivity activity, TTTGridAdapter adapter) {
        this.activity = activity;
        this.adapter = adapter;
    }

}
